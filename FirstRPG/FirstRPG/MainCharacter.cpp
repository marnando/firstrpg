#include "stdafx.h"
#include "MainCharacter.h"
#include "Sprite.h"
#include "SDL_Setup.h"

MainCharacter::MainCharacter(CSDL_Setup * a_csdl_setup, int * a_mouseX, int * a_mouseY, float * a_cameraX, float * a_cameraY)
	: m_csdl_setup(a_csdl_setup), m_mouseX(a_mouseX), m_mouseY(a_mouseY)
{
	m_distance = 0;

	//Camera movement
	m_cameraX = a_cameraX;
	m_cameraY = a_cameraY;

	//Keyboard Movement
	m_moveRight = false;
	m_moveLeft = false;
	m_moveUp = false;
	m_moveDown = false;

	//Mouse click movement
	m_follow_x = 0;
	m_follow_y = 0;
	m_follow = false;

	m_stopAnimation = false;

	//Controla o tempo para o movimento do personagem
	m_timeCheck = SDL_GetTicks();

	m_bob = new CSprite(m_csdl_setup->getRenderer(), "data/soldier.png", 300, 250, 80, 150, m_cameraX, m_cameraY);
	
	//Seta o posicionamento do ponteiro do mouse no meio do sprite
	m_bob->setOrgin(m_bob->width() / 2.0f, m_bob->height());
	
	m_bob->setUpAnimation(6, 4); //rows, cols
}


MainCharacter::~MainCharacter()
{
	if (m_bob) { delete m_bob; m_bob = NULL; }
}

void MainCharacter::draw()
{
	m_bob->drawSteady();
}

void MainCharacter::updateAnimation()
{
	//Fazer com que o movimento respeite o �ngulo de movimenta��o do click com o sprite
	// Mudou de (m_bob->x() e m_bob->y()) para (m_follow_x e *m_cameraY)
	float angle = atan2(m_follow_y - *m_cameraY, m_follow_x - *m_cameraX);
	angle = (angle * (180 / 3.14)) + 180;

	std::cout << angle << std::endl;

	//Crop frame bob
	if (angle > 45 && angle <= 135)
	{
		//down
		if (m_distance > 15)
		{
			m_bob->playAnimation(0, 5, 0, 200);
		}
		else
		{
			m_bob->playAnimation(1, 1, 0, 200);
		}
	}
	else
	if (angle > 135 && angle <= 225)
	{
		//left
		if (m_distance > 15)
		{
			m_bob->playAnimation(0, 5, 2, 200);
		}
		else
		{
			m_bob->playAnimation(1, 1, 2, 200);
		}
	}
	else
	if (angle > 225 && angle <= 315)
	{
		//up
		if (m_distance > 15)
		{
			m_bob->playAnimation(0, 5, 1, 200);
		}
		else
		{
			m_bob->playAnimation(1, 1, 1, 200);
		}
	}
	else
	if ((angle > 315 && angle <= 360) || (angle >= 0 && angle <= 45))
	{
		//right
		if (m_distance > 15)
		{
			m_bob->playAnimation(0, 5, 3, 200);
		}
		else
		{
			m_bob->playAnimation(1, 1, 3, 200);
		}
	}
}

void MainCharacter::updateControls()
{
	//Define what kind of movement will be performed
	mouseClickMove();
}

void MainCharacter::update()
{
	updateAnimation();
	updateControls();
}

double MainCharacter::getDistance(int a_x1, int a_y1, int a_x2, int a_y2)
{
	double differenceX = a_x1 - a_x2;
	double differenceY = a_y1 - a_y2;
	double distance = sqrt((differenceX * differenceX) + (differenceY * differenceY));
	return distance;
}

void MainCharacter::mouseClickMove()
{
	// Mudou de (m_bob->x() e m_bob->y()) para (m_follow_x e *m_cameraY)

	if (m_csdl_setup->getMainEvent()->type == SDL_MOUSEBUTTONDOWN
		|| m_csdl_setup->getMainEvent()->type == SDL_MOUSEMOTION)
	{
		if (m_csdl_setup->getMainEvent()->button.button == SDL_BUTTON_LEFT)
		{
			m_follow_x = *m_cameraX - *m_mouseX + 300; //300 � a prosi��o x do sprite
			m_follow_y = *m_cameraY - *m_mouseY + 250; //250 � a prosi��o y do sprite
			m_follow = true;
		}
	}

	//Controla o tempo para execu��o do comando, assim o personagem fica com movimentos mais fluidos
	if (m_timeCheck + 1 < SDL_GetTicks() && m_follow)
	{
		m_distance = getDistance(*m_cameraX, *m_cameraY, *m_mouseX, *m_mouseY);

		if (m_distance != 0)
		{
			//Change m_mouseX for m_follow_x, y too.
			if (*m_cameraX != m_follow_x)
			{
				*m_cameraX = *m_cameraX - ((*m_cameraX - m_follow_x) / m_distance) * 1.9f;
			}

			if (*m_cameraY != m_follow_y)
			{
				*m_cameraY = *m_cameraY - ((*m_cameraY - m_follow_y) / m_distance) * 1.9f;
			}
		}
		else
		{
			m_follow = false;
		}

		m_timeCheck = SDL_GetTicks();
	}
}

void MainCharacter::mouseMove()
{
	//Controla o tempo para execu��o do comando, assim o personagem fica com movimentos mais controlados
	if (m_timeCheck + 10 < SDL_GetTicks())
	{
		m_distance = getDistance(m_bob->x(), m_bob->y(), *m_mouseX, *m_mouseY);

		if (m_distance == 0)
		{
			m_stopAnimation = true;
		}
		else
		{
			m_stopAnimation = false;
		}


		if (m_distance > 15)
		{
			//Change m_mouseX for m_follow_x, y too.
			if (m_bob->x() > *m_mouseX)
			{
				m_bob->setX(m_bob->x() - ((m_bob->x() - *m_mouseX) / m_distance) * 1.9f);
			}
			if (m_bob->x() < *m_mouseX)
			{
				m_bob->setX(m_bob->x() - ((m_bob->x() - *m_mouseX) / m_distance) * 1.9f);
			}
			if (m_bob->y() > *m_mouseY)
			{
				m_bob->setY(m_bob->y() - ((m_bob->y() - *m_mouseY) / m_distance) * 1.9f);
			}
			if (m_bob->y() < *m_mouseY)
			{
				m_bob->setY(m_bob->y() - ((m_bob->y() - *m_mouseY) / m_distance) * 1.9f);
			}
		}
		else
		{
			m_follow = false;
		}

		m_timeCheck = SDL_GetTicks();
	}
}

void MainCharacter::keyMove()
{
	//Controla o tempo para execu��o do comando, assim o personagem fica com movimentos mais controlados
	if (m_timeCheck + 5 < SDL_GetTicks())
	{
		if (m_moveRight)
		{
			m_bob->setX(m_bob->x() + 5);
		}
		if (m_moveLeft)
		{
			m_bob->setX(m_bob->x() - 5);
		}
		if (m_moveUp)
		{
			m_bob->setY(m_bob->y() - 5);
		}
		if (m_moveDown)
		{
			m_bob->setY(m_bob->y() + 5);
		}

		m_timeCheck = SDL_GetTicks();
	}
}

void MainCharacter::setKeyMovement()
{
	bool unset = false;
	SDL_Keycode keyCode = SDL_Keycode();

	switch (m_csdl_setup->getMainEvent()->type)
	{
	case SDL_KEYDOWN:
	{
		keyCode = m_csdl_setup->getMainEvent()->key.keysym.sym;
	}
	break;

	case SDL_KEYUP:
	{
		keyCode = m_csdl_setup->getMainEvent()->key.keysym.sym;
		unset = true;
	}
	break;
	}

	if (!unset)
	{
		//Key down event
		switch (keyCode)
		{
		case SDLK_a:
		{
			m_moveLeft = true;
			keyMove();
		}
		break;
		case SDLK_s:
		{
			m_moveDown = true;
			keyMove();
		}
		break;
		case SDLK_w:
		{
			m_moveUp = true;
			keyMove();
		}
		break;
		case SDLK_d:
		{
			m_moveRight = true;
			keyMove();
		}
		break;
		default:
			break;
		}
	}
	else
	{
		//Key up event
		switch (keyCode)
		{
		case SDLK_a:
		{
			m_moveLeft = false;
		}
		break;
		case SDLK_s:
		{
			m_moveDown = false;
		}
		break;
		case SDLK_w:
		{
			m_moveUp = false;
		}
		break;
		case SDLK_d:
		{
			m_moveRight = false;
		}
		break;
		default:
			break;
		}
	}
}