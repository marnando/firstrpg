#pragma once

class CSprite;
class CSDL_Setup;
class MainCharacter
{

private:
	CSprite *	 m_bob;
	CSDL_Setup * m_csdl_setup;

	//Movement keyboard
	bool m_moveRight;
	bool m_moveLeft;
	bool m_moveUp;
	bool m_moveDown;

	//Mouse click event move
	bool m_follow;
	int m_follow_x;
	int m_follow_y;

	//controle de anima��o
	bool m_stopAnimation;

	//Mouse position on screen
	int * m_mouseX;
	int * m_mouseY;

	//Camera movement
	float * m_cameraX;
	float * m_cameraY;

	//Controle do tempo de movimenta��o do personagem
	int m_timeCheck;

	//para controlar a posi��o do mouse para com o sprite, melhorar o movimento
	double m_distance;


public:
	MainCharacter(CSDL_Setup * a_csdl_setup, int * a_mouseX, int * a_mouseY, float * a_cameraX, float * a_cameraY);
	~MainCharacter();
	
	/** Desenha o sprite na tela */
	void draw();

	/** Atualiza o sprite para anima��o */
	void updateAnimation();

	/** Atualiza o controle de movimenta��o, nesse caso o clique do mouse */
	void updateControls();

	/**
	Atualiza as a��es(desenho) do personagem na tela
	Ser� executado a fun��o mouseClickMove() nesse exemplo

	keyboard Move Event
	setKeyMovement();

	Mouse Move Event
	mouseMove();

	Mouse Click Event
	mouseClickMove();
	*/
	void update();

protected:

	/** Para que o movimento seja mais fl�ido entre o primeiro o �ltimo ponto de movimenta��o */
	double getDistance(int a_x1, int a_y1, int a_x2, int a_y2);

	/** Executa a a��o de movimenta��o do sprite baseado no click do mouse */
	void mouseClickMove();

	/** Executa a a��o de movimenta��o do sprite baseado no movimento do mouse */
	void mouseMove();

	/** Executa a a��o de movimenta��o do sprite baseado no digitar do teclado */
	void keyMove();

	/** Controla a ac�o do digitar do teclado e chama a fun��o keyMove() para executar o movimento so sprite
	* @a_unset Set passar true, ir� cancelar o movimento corrente, isso permite que um novo movimento seja executado
	*/
	void setKeyMovement();
};

