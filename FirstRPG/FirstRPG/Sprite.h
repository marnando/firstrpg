#pragma once
class CSprite
{
private:
	SDL_Rect m_rect;
	SDL_Rect m_crop;
	SDL_Rect m_camera;
	
	SDL_Texture *	m_image;
	SDL_Renderer *	m_renderer;

	int m_imgWidth;
	int m_imgHeight;

	int m_currentFrame;
	int m_animationDelay;
	int m_amountFrameX; //Quarda a quantidade de frames na horizontal de um sprite sheet
	int m_amountFrameY; //Quarda a quantidade de frames na vertical de um sprite sheet

	//Camera movement
	float * m_cameraX;
	float * m_cameraY;

	double m_x_pos;
	double m_y_pos;
	double m_orginX;
	double m_orginY;

public:
	CSprite(SDL_Renderer * a_renderer, std::string a_filePath, int a_x, int a_y, int a_w, int a_h, float * a_cameraX, float * a_cameraY);
	~CSprite();

	/** Para que a o sprite seja renderizado com influ�ncia da cam�ra */
	void draw();
	
	/** Para que a o sprite seja renderizado sem influ�ncia da cam�ra, somente o rect */
	void drawSteady();

	/** Informa a quantidade de frames nas linhas e colunas de um sprite sheet */
	void setUpAnimation(int a_amountX, int a_amountY);
	void playAnimation(int a_beginFrame, int a_endFrame, int a_row, float a_speed);
	
	void setX(double a_x);
	void setY(double a_y);
	void setPosition(double a_x, double a_y);

	/** Centraliza o ponteiro do mouse no meio do sprite */
	void setOrgin(double a_x, double a_y);

	double x();
	double y();

	void setWidth(int a_w);
	void setHeight(int a_h);

	int width();
	int height();
};

