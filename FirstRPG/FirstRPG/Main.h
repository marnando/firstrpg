#pragma once

class MainCharacter;
class CSDL_Setup;
class CSprite;
class CEnvironment;
class CMain
{
private:
	MainCharacter * m_bob;
	CSDL_Setup * m_csdl_setup;
	CSprite *	 m_grass[4][7]; //Montando o cen�rio com a grama renderizada em uma matriz
	CEnvironment * m_environment;

	bool m_quit;

	//Movement mouse
	int m_mouseX;
	int m_mouseY;

	int m_screenWidth;
	int m_screenHeight;

	//Camera
	float m_cameraX;
	float m_cameraY;

public:
	CMain(int a_screenWidth, int a_screenHeight);
	~CMain();

	//Loop principal da aplica��o
	void gameLoop();
};

