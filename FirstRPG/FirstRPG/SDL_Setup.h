#pragma once
class CSDL_Setup
{
private:
	CSDL_Setup *	m_sdlSetup;

	SDL_Window *	m_window;
	SDL_Renderer *	m_renderer;
	SDL_Event *		m_mainEvent;

public:
	CSDL_Setup(bool * a_quit, int a_screenWidth, int a_screenHeight);
	~CSDL_Setup();

	void begin();
	void end();

	SDL_Renderer * getRenderer();
	SDL_Window * getWindow();
	SDL_Event * getMainEvent();
};

