#include "stdafx.h"
#include "SDL_Setup.h"


CSDL_Setup::CSDL_Setup(bool * a_quit, int a_screenWidth, int a_screenHeight)
	: m_window(0)
	, m_renderer(0)
	, m_mainEvent(new SDL_Event())
{
	//Inicializando as fun��es de v�dio, inclui �udio e tudo mais. Caso queira inicializar tudo usa a flag SDL_INIT_EVERYTHING
	SDL_Init(SDL_INIT_VIDEO);

	//Criando a janela da aplica��o
	m_window = SDL_CreateWindow("My First RPG!", 100, 100, a_screenWidth, a_screenHeight, SDL_WINDOW_SHOWN); //FULLSCREEN, RESIZABLE, ETC ...

	//Para caso o programa crash no iniciar a aplica��o, encerra
	if (m_window == NULL)
	{
		std::cout << "Window couldn't be created!" << std::endl;
		*a_quit = true;
	}

	//Para que a aplica��o n�o fique travada no loop proncipal, � necess�rio que seja atualizado a renderiza��o da tela
	m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED);
}


CSDL_Setup::~CSDL_Setup()
{
	SDL_DestroyWindow(m_window);
	SDL_DestroyRenderer(m_renderer);

	delete m_mainEvent;
	m_mainEvent = NULL;
}

void CSDL_Setup::begin()
{
	//Adiciona os eventos para serem processados, fazendo o while, pode-se trabalhar com os eventos
	SDL_PollEvent(m_mainEvent);
	//Limpa a tela para que seja poss�vel esolher novas cores.
	SDL_RenderClear(m_renderer);
}

void CSDL_Setup::end()
{
	//Redesenha todos os elementos do cen�rio, renderizando.
	SDL_RenderPresent(m_renderer);
}

SDL_Renderer * CSDL_Setup::getRenderer()
{
	return m_renderer;
}

SDL_Window * CSDL_Setup::getWindow()
{
	return m_window;
}

SDL_Event * CSDL_Setup::getMainEvent()
{
	return m_mainEvent;
}