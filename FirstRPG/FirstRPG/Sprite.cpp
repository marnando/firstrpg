#include "stdafx.h"
#include "Sprite.h"

CSprite::CSprite(SDL_Renderer * a_renderer, std::string a_filePath, int a_x, int a_y, int a_w, int a_h, float * a_cameraX, float * a_cameraY)
	: m_image(0), m_renderer(a_renderer)
{
	//Carregar a imagem como background para a tela
	m_image = IMG_LoadTexture(m_renderer, a_filePath.c_str());

	if (m_image == NULL)
	{
		std::cout << "Couldn't load " << a_filePath.c_str() << std::endl;
	}

	//Dimensionando a imagem
	m_rect.x = a_x;
	m_rect.y = a_y;
	m_rect.w = a_w;
	m_rect.h = a_h;

	//Captura o tamanho real do arquivo
	SDL_QueryTexture(m_image, NULL, NULL, &m_imgWidth, &m_imgHeight);

	//Corte da imagem para anima��o
	m_crop.x = 0;
	m_crop.y = 0;
	m_crop.w = m_imgWidth;
	m_crop.h = m_imgHeight;

	m_currentFrame = 0;
	m_amountFrameX = 0;
	m_amountFrameY = 0;

	m_x_pos = a_x;
	m_y_pos = a_y;

	// Para alinhamento do ponteiro do mouse com sprite
	m_orginX = 0;
	m_orginY = 0;

	//Camera movement
	m_cameraX = a_cameraX;
	m_cameraY = a_cameraY;

	m_camera.x = m_rect.x + *m_cameraX;
	m_camera.y = m_rect.y + *m_cameraY;
	m_camera.w = m_rect.w;
	m_camera.h = m_rect.h;
}

CSprite::~CSprite()
{
	//Ap�s o la�o principal � necess�rio matar os ponteiros da aplica��o (SDL)
	SDL_DestroyTexture(m_image);
}

void CSprite::draw()
{
	m_camera.x = m_rect.x + *m_cameraX;
	m_camera.y = m_rect.y + *m_cameraY;

	//SDL_Rect Position;
	//Position.x = m_rect.x + *m_cameraX;
	//Position.y = m_rect.y + *m_cameraY;
	//Position.w = m_rect.w;
	//Position.h = m_rect.h;

	//Desenha os elementos para a tela
	SDL_RenderCopy(m_renderer, m_image, &m_crop, &m_camera);
}

void CSprite::drawSteady()
{
	//Desenha os elementos para a tela
	SDL_RenderCopy(m_renderer, m_image, &m_crop, &m_rect);
}

void CSprite::setUpAnimation(int a_amountX, int a_amountY)
{
	m_amountFrameX = a_amountX;
	m_amountFrameY = a_amountY;
}

void CSprite::playAnimation(int a_beginFrame, int a_endFrame, int a_row, float a_speed)
{
	if (m_animationDelay + a_speed < SDL_GetTicks())
	{
		//Verifica a posi��o atual do frame e passa para o pr�ximo no pr�ximo movimento
		if (a_endFrame <= m_currentFrame)
		{
			m_currentFrame = a_beginFrame;
		}
		else
		{
			m_currentFrame++;
		}

		//Atualiza o corte do sprite sheet para o prox de acordo com a posi��o no vetor
		m_crop.x = m_currentFrame * (m_imgWidth / m_amountFrameX);
		m_crop.y = a_row * (m_imgHeight / m_amountFrameY);
		m_crop.w = m_imgWidth / m_amountFrameX;
		m_crop.h = m_imgHeight / m_amountFrameY;

		m_animationDelay = SDL_GetTicks();
	}
}

void CSprite::setX(double a_x)
{
	m_x_pos = a_x;
	m_rect.x = int(m_x_pos - m_orginX);
}

void CSprite::setY(double a_y)
{
	m_y_pos = a_y;
	m_rect.y = int(m_y_pos - m_orginY);
}

void CSprite::setPosition(double a_x, double a_y)
{
	m_x_pos = a_x;
	m_y_pos = a_y;

	m_rect.x = int(m_x_pos - m_orginX);
	m_rect.y = int(m_y_pos - m_orginY);
}

void CSprite::setOrgin(double a_x, double a_y)
{
	m_orginX = a_x;
	m_orginY = a_y;

	setPosition(x(), y());
}

double CSprite::x()
{
	return m_x_pos;
}

double CSprite::y()
{
	return m_y_pos;
}

void CSprite::setWidth(int a_w)
{
	m_rect.w = a_w;
}

void CSprite::setHeight(int a_h)
{
	m_rect.h = a_h;
}

int CSprite::width()
{
	return m_rect.w;
}

int CSprite::height()
{
	return m_rect.h;
}