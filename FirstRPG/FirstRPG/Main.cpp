#include "stdafx.h"
#include "Main.h"
#include "SDL_Setup.h"
#include "Sprite.h"
#include "Environment.h"
#include "MainCharacter.h"

CMain::CMain(int a_screenWidth, int a_screenHeight) 
	: m_csdl_setup(0)
	, m_bob(0)
	, m_environment(0)
{
	m_quit = false;

	//Mouse movement
	m_mouseX = 0;
	m_mouseY = 0;

	m_cameraX = 0.0;
	m_cameraY = 0.0;

	m_screenWidth = a_screenWidth;
	m_screenHeight = a_screenHeight;

	m_csdl_setup = new CSDL_Setup(&m_quit, m_screenWidth, m_screenHeight);

	m_environment = new CEnvironment(m_csdl_setup, m_screenWidth, m_screenHeight, &m_cameraX, &m_cameraY);

	//Inicia os personagens
	m_bob = new MainCharacter(m_csdl_setup, &m_mouseX, &m_mouseY, &m_cameraX, &m_cameraY);
	
}

CMain::~CMain()
{
	if (m_csdl_setup)	{ delete m_csdl_setup; m_csdl_setup = NULL; }
	if (m_bob)			{ delete m_bob; m_bob = NULL; }
	if (m_environment)	{ delete m_environment; m_environment = NULL; }
}	

void CMain::gameLoop()
{
	while (!m_quit 
	   &&  (m_csdl_setup->getMainEvent()->type != SDL_QUIT)) // Sai com quit sendo verdadeiro ou quanto aperta o esc
	{
		//Inicio a aplica��o
		m_csdl_setup->begin();

		// Pega a posi��o atual do mouse, caso utilize o evento do mouse
		SDL_GetMouseState(&m_mouseX, &m_mouseY);

		//Desenho os elementos da tela
		m_environment->drawBack();
		m_bob->draw();
		m_bob->update();

		//Encerro a aplica��o
		m_csdl_setup->end();
	}
}



