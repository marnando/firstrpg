#pragma once
#include "stdafx.h"

class CSprite;
class CSDL_Setup;
class CEnvironment
{
private:
	CSDL_Setup * m_cSdlSetup;
	CSprite *	 m_grass[4][7]; //Montando o cen�rio com a grama renderizada em uma matriz

public:
	CEnvironment(CSDL_Setup * a_cSdlSetup, int a_screenWidth, int a_screenHeight, float * a_cameraX, float * a_cameraY);
	~CEnvironment();

	void drawBack();
	void drawFront();
};

