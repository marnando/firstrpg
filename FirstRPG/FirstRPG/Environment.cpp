#include "stdafx.h"
#include "Sprite.h"
#include "SDL_Setup.h"
#include "Environment.h"

CEnvironment::CEnvironment(CSDL_Setup * a_cSdlSetup, int a_screenWidth, int a_screenHeight, float * a_cameraX, float * a_cameraY)
{
	m_cSdlSetup = a_cSdlSetup;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 7; j++)
		{
			m_grass[i][j] = new CSprite(m_cSdlSetup->getRenderer()
								 	  , "data/environment/grass.bmp"
									  , a_screenWidth * i
									  , a_screenHeight * j
									  , a_screenWidth
									  , a_screenHeight
									  , a_cameraX
									  , a_cameraY);
		}
	}
}

CEnvironment::~CEnvironment()
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 7; j++)
		{
			delete m_grass[i][j];
			m_grass[i][j] = NULL;
		}
	}
}

void CEnvironment::drawBack()
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 7; j++)
		{
			m_grass[i][j]->draw();
		}
	}
}

void CEnvironment::drawFront()
{

}
